import logging
import time
from typing import List, Optional, cast

from langchain.docstore.document import Document
from langchain.document_loaders import UnstructuredPDFLoader
from pdfminer.high_level import extract_text
from pdfminer.layout import LAParams
from PIL.Image import Image as PilImage
from unstructured.partition.pdf import convert_pdf_to_images

from src.gdrive_loader.azure_vision import azure_read_img
from src.ocr.merge_text_boxes_strategies import greedy_grow

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


class CustomPDFLoader(UnstructuredPDFLoader):
    _is_pdf_text_extractable: Optional[bool] = None

    @property
    def is_pdf_text_extractable(self) -> bool:
        if self._is_pdf_text_extractable is not None:
            return self._is_pdf_text_extractable

        file_path = cast(str, self.file_path)
        logger.info("Determining if pdf %s is extractable ...", file_path)
        start = time.time()

        # Use a custom LAParams to avoid any extra processing
        laparams = LAParams(all_texts=True)

        # Extract text from the PDF
        # NOTE: This takes time, maybe change to using the fitz module
        extracted_text = extract_text(file_path, laparams=laparams)
        # Check if the extracted text is non-empty
        self._is_pdf_text_extractable = bool(extracted_text.strip())

        end = time.time()
        logger.info(
            "pdf %s is extractable: %s. Took %ss",
            file_path,
            self._is_pdf_text_extractable,
            end - start,
        )

        return self._is_pdf_text_extractable

    def read_single_image(self, image_path: str, img_id: str | None = None) -> Document:
        """
        img_id: name of the image to appear on the logs.

        You can replace this function by any alternative. For e.g
        - AmazonTextractPDFLoader from langchain, which only takes single page PDF
        in input.
        """
        text_boxes = azure_read_img(image_path)

        logger.info("Merging %s bboxes using greedy_grow ...", img_id)
        start = time.time()
        # Normally should only have 1 text_box left after greedy_grow
        merged_text_boxes = greedy_grow(text_boxes)
        end = time.time()
        logger.info(
            "Merged %s bboxes using greedy_grow . Took %ss", img_id, end - start
        )

        # Copied from the original load method
        metadata = self._get_metadata()
        text = "\n\n".join([x.content for x in merged_text_boxes])
        doc = Document(page_content=text, metadata=metadata)
        return doc

    def load(self) -> List[Document]:
        result: List[Document] = []
        if self.is_pdf_text_extractable:
            logger.info(
                "PDF text is extractable, falling back to original load method."
            )
            return super().load()

        logger.info("Extracting text from PDF %s ...", self.file_path)
        start = time.time()

        for i, image in enumerate(convert_pdf_to_images(self.file_path)):
            image = cast(PilImage, image)
            img_id = f"{self.file_path}:page_{i + 1}"

            start_image = time.time()
            logger.info(f"Extracting {img_id} ...")

            # Saving each image as pdf file. Since we need to give a path
            # to the reader, we need to save the img beforehand.
            filepath = cast(str, self.file_path)
            temp_pdf_path = filepath.split(".")[0] + "_" + str(i + 1) + ".png"
            image.save(temp_pdf_path)

            # Read image
            result.append(self.read_single_image(temp_pdf_path, img_id=img_id))
            end_image = time.time()
            logger.info(f"Extracted {img_id}. Took {end_image - start_image}s")

        end = time.time()
        logger.info(
            "Extracted text from PDF %s. Took %ss",
            self.file_path,
            end - start,
        )

        # Then merge all pages into one single document
        text = "\n\n".join([x.page_content for x in result])
        return [Document(page_content=text, metadata=result[0].metadata)]
