from typing import List, cast

from langchain.docstore.document import Document
from langchain.document_loaders import UnstructuredImageLoader

from src.gdrive_loader.azure_vision import azure_read_img
from src.ocr.merge_text_boxes_strategies import greedy_grow


class CustomImageLoader(UnstructuredImageLoader):
    def _get_elements(self) -> List:
        raise ValueError("_get_elements should not be called")

    def load(self) -> List[Document]:
        text_boxes = azure_read_img(cast(str, self.file_path))

        # Normally should only have 1 text_box left
        merged_text_boxes = greedy_grow(text_boxes)

        # Copied from the original load method
        metadata = self._get_metadata()
        text = "\n\n".join([x.content for x in merged_text_boxes])
        docs = [Document(page_content=text, metadata=metadata)]
        return docs
