import logging
import os
import time
from typing import List, TypedDict, Union, cast

import requests

from src.exceptions.http_exceptions import AzureVisionReadException
from src.ocr.text_boxes import TextBox
from src.settings import settings

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


# Classes to type the result of Azure vision
class WithConfidence(TypedDict):
    confidence: float


class AzureTextBox(TypedDict):
    content: str
    boundingBox: List[float]  # 8elements


class AzurePage(TypedDict):
    height: float
    width: float
    angle: float
    pageNumber: int
    words: List[Union[AzureTextBox, WithConfidence]]
    lines: List[AzureTextBox]


class AzureReadResult(TypedDict):
    stringIndexType: str
    # the words concatenated into a string string
    # However it's a naive concatenation from left to right, top to bottom
    content: str
    pages: List[AzurePage]


class AzureResult(TypedDict):
    readResult: AzureReadResult


def azure_read_img(image_path: str) -> List[TextBox]:
    key = settings.VISION_KEY
    endpoint = settings.VISION_ENDPOINT
    headers = {
        # Request headers
        "Ocp-Apim-Subscription-Key": key,
        "Content-Type": "application/octet-stream",
    }

    features = "read"
    model_version = "latest"
    language = "en"
    api_version = "2023-02-01-preview"

    with open(image_path, "rb") as img_stream:
        # We are using the API endpoint directly because the SDK has some issues
        # https://github.com/Azure-Samples/azure-ai-vision-sdk/issues/40#issuecomment-1638669858

        logger.info("Reading img %s on AzureVision ...", image_path)
        start = time.time()
        azure_vision_url = os.path.join(
            endpoint,
            "computervision/imageanalysis:analyze?"
            + f"features={features}"
            + f"&model-version={model_version}"
            + f"&language={language}"
            + f"&api-version={api_version}",
        )
        response = requests.post(
            url=azure_vision_url,
            headers=headers,
            data=img_stream,
        )
        end = time.time()
        logger.info(
            "Finished reading img %s on AzureVision. Took %ss", image_path, end - start
        )

    if response.status_code == 200:
        data = cast(AzureReadResult, response.json()["readResult"])
    else:
        logger.error(
            "Unexpected error when reading img %s from AzureVision. Error is %s",
            image_path,
            response.json(),
        )
        raise AzureVisionReadException()

    # Parse content into TextBox
    # Should only have 1 page
    page = data["pages"][0]
    text_boxes: List[TextBox] = []
    for line in page["lines"]:
        line_content = line["content"]
        line_bbox = line["boundingBox"]
        p11 = (line_bbox[0], line_bbox[1])
        p22 = (line_bbox[4], line_bbox[5])
        text_boxes.append(TextBox(content=line_content, p11=p11, p22=p22))

    return text_boxes
