import logging
import os
import time
from typing import Any, List, TypedDict

from src.gdrive_loader.catch_exceptions import (
    catch_gdrive_load_files_errors,
    catch_google_credentials_errors
)

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


class GdriveListItems(TypedDict):
    id: str
    mimeType: str
    path_id: str


# Function to get the parent folder IDs recursively.
def get_parent_path(service: Any, folder_id: str) -> str:
    """
    Given a folder_id, it will fetch the whole parent hierarchy to the root.

    The purpose of this functions is because the _list_subfolder_hierarchy
    function only returns the hierarchy for subfolders of folder_id.

    However, it is not guaranteed that `folder_id` provided will be the root
    folder. In cases where you need the info of the parent hierarchy, this
    functions is here for you.

    Ex: if you have A -> B -> C -> D, and you provide `folder_id=C`, then
    it will return:  "A/B"

    If folder_id is already at the root, returns ""
    """
    parent_ids: List[str] = []
    cursor = folder_id
    logger.info(f"Loading parent path for {folder_id}...")

    start = time.time()
    while True:
        # NOTE: Should we add a counter of iteration and stop if too many iteration ?
        # This would be a safety net for the while: True loop.
        with catch_google_credentials_errors(), catch_gdrive_load_files_errors(
            log_message_if_error=(
                f"Error when loading gdrive parent_path of folder {folder_id}."
            )
        ):
            folder_info = (
                service.files()
                .get(
                    fileId=cursor,
                    fields="parents",
                    supportsAllDrives=True,
                )
                .execute()
            )

        parents = folder_info.get("parents", [])

        # Google returns empty dict if we have reached the root
        if not parents:
            break

        # Add newly found parent to the path
        parent_ids = [parents[0]] + parent_ids
        cursor = parents[0]

    end = time.time()
    logger.info(f"Finished loading parent path for {folder_id}. Took {end - start}s")
    return "/".join(parent_ids)


# The loader from langchain does not return the id of the parent folder of each document.
# We need the whole
# Function to list files and folders recursively.
def list_subfolders_hierarchy(
    service: Any, folder_id: str, parent_path_id: str = ""
) -> List[GdriveListItems]:
    """
    Given a folder_id, it returns the list of subfolders & files with their path.
    We are forced to do recursive calls to the API because Gdrive does not
    explicitly display the hierarchy of your folders.
    """

    logger.info(f"Loading subfolders hierarchy for {folder_id}...")
    start = time.time()

    # I'm putting the real code in an auxiliary function because
    # __list_subfolders_hierarchy__ is recursive, and I didn't want the log
    # to appear in each recursive loop.
    results = __list_subfolders_hierarchy__(service, folder_id, parent_path_id)

    end = time.time()
    logger.info(
        f"Finished loading subfolder hierarchy for {folder_id}. Took {end - start}s"
    )
    return results


def __list_subfolders_hierarchy__(
    service: Any, folder_id: str, parent_path_id: str = ""
) -> List[GdriveListItems]:
    current_path = os.path.join(parent_path_id, folder_id)
    results = []

    with catch_google_credentials_errors(), catch_gdrive_load_files_errors(
        log_message_if_error=(
            f"Error when loading gdrive subfolder hierarchy of folder {folder_id}."
        )
    ):
        items = (
            service.files()
            .list(
                q=f"'{folder_id}' in parents",
                includeItemsFromAllDrives=True,
                supportsAllDrives=True,
                fields="files(id, mimeType)",
            )
            .execute()
        ).get("files", [])

    for item in items:
        item["path_id"] = f"{current_path}/{item['id']}"

    results += items

    for item in items:
        # If the item is a folder, recursively list its contents.
        if item["mimeType"] == "application/vnd.google-apps.folder":
            results += __list_subfolders_hierarchy__(
                service, item["id"], parent_path_id=current_path
            )

    return results
