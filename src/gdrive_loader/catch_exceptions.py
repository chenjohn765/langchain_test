import logging
from contextlib import contextmanager
from typing import Generator

from google.auth.exceptions import RefreshError
from googleapiclient.errors import HttpError as GoogleHttpError

from src.exceptions.http_exceptions import (
    GdriveConnectionFailed,
    UnexpectedLoadingGoogleDriveException,
    UnknownGooleDriveFolderException
)

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


@contextmanager
def catch_google_credentials_errors() -> Generator:
    """
    Context manager to try/catch errors linked to invalid credentials.
    """
    try:
        yield
    except RefreshError as e:
        logger.error(
            "Error occcured when refreshing google access_token. Error is: %s", e
        )
        raise GdriveConnectionFailed()


@contextmanager
def catch_gdrive_load_files_errors(log_message_if_error: str = "") -> Generator:
    """
    Context manager to try/catch errors raised when calling Gdrive's API.
    """
    try:
        yield
    except GoogleHttpError as e:
        logger.error(log_message_if_error + " Error is {e}")
        if e.status_code == 404:
            raise UnknownGooleDriveFolderException()
        raise UnexpectedLoadingGoogleDriveException()
