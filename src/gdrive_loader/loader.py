import io
import json
import logging
from functools import partial
from pathlib import Path
from typing import Any, Dict, Iterator, List, Optional

from langchain.docstore.document import Document
from langchain.document_loaders import (
    UnstructuredExcelLoader,
    UnstructuredFileLoader
)
# Use the advanced version.
from langchain_googledrive.document_loaders import \
    GoogleDriveLoader as GoogleDriveLoaderEnhanced
from langchain_googledrive.document_loaders.google_drive import (
    GoogleDriveUtilities
)
from langchain_googledrive.utilities.google_drive import (
    TYPE_CONV_MAPPING,
    Field,
    default_conv_loader
)

from src.gdrive_loader.image_loader import CustomImageLoader
from src.gdrive_loader.pdf_loader import CustomPDFLoader

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


SCOPES = ["https://www.googleapis.com/auth/drive.readonly"]


def fixed_conv_loader(*args: Any, **kwargs: Any) -> TYPE_CONV_MAPPING:
    excel_mimetype = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    conv_mapping = default_conv_loader(*args, **kwargs)
    conv_mapping.update({excel_mimetype: UnstructuredExcelLoader})

    # The default_conv_loader will raise warnings saying it will ignore images
    # Because it does not have the correct dependencies
    # But given we load images using our own method, this warning should be ignored.
    logger.info("Adding CustomImageLoader. Can now load image from Gdrive.")
    conv_mapping.update(
        {
            "image/png": partial(CustomImageLoader, ocr_languages="eng"),
            "image/jpeg": partial(CustomImageLoader, ocr_languages="eng"),
            "application/json": partial(UnstructuredFileLoader, ocr_languages="eng"),
        }
    )
    logger.info("Adding CustomPDFLoader. Can now load PDF from Gdrive.")
    conv_mapping.update(
        {
            "application/pdf": partial(CustomPDFLoader, strategy="fast", mode="single"),
        }
    )
    return conv_mapping


class FixedGoogleDriveUtilities(GoogleDriveUtilities):
    """
    This class will fix an error when the initial class would raise
    if you provide a web_app type credentials .json file as `credentials_path`
    even if it's not actively using it.

    That is because, the original code would assume you use only either
    desktop_app type of file or service_account type of file,
    and will try to parse your file as `service_account` type
    if not detected as `desktop_app`.

    If you provide a `token_path` field for this class, it will not use
    the `credentials_path` file provided, but the file will still be parsed
    and raise an error if it is not of the correct format.

    This fix is to avoid raising this error if token_path is provided.
    """

    #  Copied the initial method from the lib. Then added some fixes.
    def _load_credentials(self, api_file: Optional[Path], scopes: List[str]) -> Any:
        """Load credentials.

         Args:
            api_file: The user or services json file

        Returns:
            credentials.
        """
        try:
            from google.auth.transport.requests import Request
            from google.oauth2 import service_account
            from google.oauth2.credentials import Credentials  # type: ignore
            from google_auth_oauthlib.flow import \
                InstalledAppFlow  # type: ignore
        except ImportError:
            raise ImportError(
                "You must run "
                "`pip install --upgrade "
                "google-api-python-client google-auth-httplib2 "
                "google-auth-oauthlib` "
                "to use the Google Drive loader."
            )

        # ================= Initial code ================================
        # if api_file:
        #     with io.open(api_file, "r", encoding="utf-8-sig") as json_file:
        #         data = json.load(json_file)
        #     if "installed" in data:
        #         credentials_path = api_file
        #         service_account_key = None
        #     else:
        #         service_account_key = api_file
        #         credentials_path = None
        # else:
        #     raise ValueError("Use GOOGLE_ACCOUNT_FILE env. variable.")

        # ================= Fixed code ================================
        if api_file:
            with io.open(api_file, "r", encoding="utf-8-sig") as json_file:
                data: Dict = json.load(json_file)
            if "type" in data and data.get("type") == "service_account":
                service_account_key = api_file
                credentials_path = None
            else:
                credentials_path = api_file
                service_account_key = None
        else:
            raise ValueError("Use GOOGLE_ACCOUNT_FILE env. variable.")

        # ===============================================================

        # Implicit location of token.json
        if not self.gdrive_token_path and credentials_path:
            token_path = credentials_path.parent / "token.json"

        creds = None
        if service_account_key and service_account_key.exists():
            return service_account.Credentials.from_service_account_file(
                str(service_account_key), scopes=scopes
            )

        if token_path.exists():
            creds = Credentials.from_authorized_user_file(str(token_path), scopes)

        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    str(credentials_path), scopes
                )
                creds = flow.run_local_server(port=0)
            with open(token_path, "w") as token:
                token.write(creds.to_json())

        return creds


class FixedGoogleDriveLoaderEnhanced(
    GoogleDriveLoaderEnhanced, FixedGoogleDriveUtilities
):
    conv_mapping: TYPE_CONV_MAPPING = Field(default_factory=fixed_conv_loader)
    """A dictionary to map a mime-type and a loader"""

    # Added possibilitity to add kwargs to methods
    # This is because the original method of lazy_get_relevant_documents
    # is missing `includeItemsFromAllDrives` option to fetch files
    # from shared drive.
    def lazy_load(self, **kwargs: Any) -> Iterator[Document]:
        return self.lazy_get_relevant_documents(**kwargs)

    def load(self, **kwargs: Any) -> List[Document]:
        return list(self.lazy_load(**kwargs))
