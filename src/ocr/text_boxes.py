# We ignore the line too long for this file as this is for promts
# It's expected to have very long lines.
# flake8: noqa: E501

import uuid
from typing import Dict, List, Tuple, TypedDict, cast

from pydantic import BaseModel, Field

# We assume that a rectangle is formed by 4 points:
#  p11 ------------------ p12
#   |                       |
#  p21 -------------------p22
#
#  However, it only needs p11 & p22 to be defined.
#
# A point such as p11 is defined by his tuple (x, y)
#
# Axis are the following
# 0 ----> x
# |
# |
# y

# Coordinate is just an alias so we can easily switch between
# int or float if needed
Coordinate = float
Point = Tuple[Coordinate, Coordinate]
Rectangle = Tuple[Point, Point]
# RawRectangle is just rectangle but flatten
RawRectangle = Tuple[Coordinate, Coordinate, Coordinate, Coordinate]


class GroupMemo(TypedDict):
    # dictionnary is of following format
    # { text_box.id => merge_id}
    merge_ids: Dict[str, str]
    # dictionnary is of following format
    # { merge_id => [text_box1, text_box2, ..]}
    groups: Dict[str, List["TextBox"]]


def create_new_group_memo() -> GroupMemo:
    return {"groups": {}, "merge_ids": {}}


class TextBox(BaseModel):
    content: str
    id: str = Field(default_factory=lambda: str(uuid.uuid4()))
    p11: Point
    p22: Point

    def group_with(self, other: "TextBox", group_memo: GroupMemo) -> None:
        """
        We follow a "greedy" pattern of grouping. Let's say we have:
        t1, t2    t3, t4, t5

        This method should be called when the function calculate_distance
        estimates that t3 & t4 are close enough.

        In the same way, this function might be called for t4 & t5, but may
        or may not be called for t3 & t5 (if it estimates they are too far).
        By transitivity, at the end, we want to group t3, t4 & t5 altogether.

        This function does not actually merge the boxes, but mark them with a merge_id
        to indicate they all belong to the same group to merge together.

        The reason being that, we can't merge boxes 2 by 2. E.g:
        merge(merge(t3, t5), t4) != merge(t3, t4, t5)
        This is because the texts must be joined in the right order.

        This is why we use this system of tagging the boxes with a merge_id,
        and we keep all of this info in an external `group_memo`.
        This is so we avoid setting an attribute `merge_id` that would mutate
        this class instance.
        """
        groups = group_memo["groups"]
        merge_ids = group_memo["merge_ids"]
        self_merge_id = merge_ids.get(self.id)
        other_merge_id = merge_ids.get(other.id)

        if not self_merge_id and other_merge_id:
            # This means self must be absorbe by other
            groups[other_merge_id].append(self)
            merge_ids[self.id] = other_merge_id
        elif self_merge_id and not other_merge_id:
            # This means self must absorb other
            groups[self_merge_id].append(other)
            merge_ids[other.id] = self_merge_id
        elif not self_merge_id and not other_merge_id:
            # This means they are both "individuals"
            # We will create a new group with both of them inside
            # We simply put the id of the textbox as merge_id
            new_merge_id = self.id
            merge_ids[self.id] = new_merge_id
            merge_ids[other.id] = new_merge_id
            groups[new_merge_id] = [self, other]
        else:
            # Mypy doesnt see to be able to detect that
            # self_merge_id and other_merge_id are both set
            self_merge_id = cast(str, self_merge_id)
            other_merge_id = cast(str, other_merge_id)

            # This means that self & other both belong to a group
            if self_merge_id == other_merge_id:
                return

            # If both groups are different, then one must absorb the other
            # The bigger group will absorb the smaller group
            if len(groups[self_merge_id]) >= len(groups[other_merge_id]):
                groups[self_merge_id] += groups[other_merge_id]
                del groups[other_merge_id]

                # We need to update the merge_id of all the boxes
                # that were ansorbed
                for tb_id in merge_ids:
                    if merge_ids[tb_id] == other_merge_id:
                        merge_ids[tb_id] = self_merge_id
            else:
                groups[other_merge_id] += groups[self_merge_id]
                del groups[self_merge_id]
                # We need to update the merge_id of all the boxes
                # that were ansorbed
                for tb_id in merge_ids:
                    if merge_ids[tb_id] == self_merge_id:
                        merge_ids[tb_id] = other_merge_id

    @property
    def p21(self) -> Point:
        return (self.p11[0], self.p22[1])

    @property
    def p12(self) -> Point:
        return (self.p22[0], self.p11[1])

    @property
    def min_x(self) -> Coordinate:
        return self.p11[0]

    @property
    def min_y(self) -> Coordinate:
        return self.p11[1]

    @property
    def max_x(self) -> Coordinate:
        return self.p22[0]

    @property
    def max_y(self) -> Coordinate:
        return self.p22[1]

    @property
    def box(self) -> Rectangle:
        return (self.p11, self.p22)

    @property
    def center(self) -> Tuple[float, float]:
        return (self.p11[0] + self.p22[0]) / 2, (self.p11[1] + self.p22[1]) / 2

    @property
    def box_raw(self) -> RawRectangle:
        return (self.p11[0], self.p11[1], self.p22[0], self.p22[1])

    def __lt__(self, tb2: "TextBox") -> bool:
        """
        Will allow sorting of text_boxes. Being lower means you have priority.
        Order is left to right. Top to bottom.
        """
        if self.center[1] < tb2.min_y:
            # self.center is completely above tb2's top edge
            # (on top)         (not perfectly above)        (not perfectly above + overlapping)
            #   -------          -------                         -------
            #   |      |         |      |                        |      |
            #   |  t1  |         |  t1  |    -------             |  t1  |
            #   |______|   OR    |______|    |      |  OR      --|------|--
            #   -------                      |  t2  |          | |______|  |
            #   |      |                     |______|          |    t2     |
            #   |  t2  |                                       |___________|
            #   |______|
            return True
        elif tb2.center[1] < self.min_y:
            # tb2.center is completely above self's top edge
            # (on top)         (not perfectly above)        (not perfectly above + overlapping)
            #   -------          -------                         -------
            #   |      |         |      |                        |      |
            #   |  t2  |         |  t2  |    -------             |  t2  |
            #   |______|   OR    |______|    |      |  OR      --|------|--
            #   -------                      |  t1  |          | |______|  |
            #   |      |                     |______|          |    t1     |
            #   |  t1  |                                       |___________|
            #   |______|
            return False

        # If code has reached here, it means that the two text_boxes
        # are about the same horizontal line.

        # CENTER will not work for the cases below,
        # for example, if one text_box is envelopping another
        # Then this will might not hold true if the center of
        # the bigger one is on the left
        elif self.max_x < tb2.max_x:
            # self is on the left of tb2
            #   (separated)               (overlapping)             (included)
            #                                   -----------       --------------
            #   -------    -------        ------|-        |       |     -----   |
            #   |      |   |      |       |     | |       |       |     | t1 |  |
            #   |  t1  |   |  t2  |  OR   |  t1 | |   t2  |  OR   |     |____|  |
            #   |______|   |______|       |_____|_|       |       |             |
            #                                   |_________|       |     t2      |
            #                                                     |             |
            #                                                     |_____________|
            return True

        elif tb2.max_x < self.max_x:
            # tb2 is on the left of self
            # self is on the left of tb2
            #   (separated)               (overlapping)             (included)
            #                                   -----------       --------------
            #   -------    -------        ------|-        |       |     -----   |
            #   |      |   |      |       |     | |       |       |     | t2 |  |
            #   |  t2  |   |  t1  |  OR   |  t2 | |   t1  |  OR   |     |____|  |
            #   |______|   |______|       |_____|_|       |       |             |
            #                                   |_________|       |     t1      |
            #                                                     |             |
            #                                                     |_____________|
            return False

        # ======== From here, should be very rare cases =====================
        # If code is reached here, they have the same max_x,
        # Meaning right edge is overlapping
        elif self.max_y < tb2.max_y:
            # self should be before because it "ends" before
            # The bottom edge is before t2's bottom edge
            #
            #     (included)        (a bit outside)          (a bit outside vertically)
            #   ---------------     ---------------
            #   |     --------|   --|-------------|            ---------
            #   |     | t1    |   | |       t1    |            |  t1    |
            #   |     |       |   |_|_____________|            |        |
            #   |     |       |     |             |        ----|--------|
            #   |  t2 |       |     |     t2      |       |    | t2     |
            #   |     |_______|     |             |       |    |________|
            #   |_____________|     |_____________|       |_____________|
            return True
        elif tb2.max_y < self.max_y:
            # t2 should be before because it "ends" before
            # The bottom edge is before self's bottom edge
            #
            #     (included)        (a bit outside)          (a bit outside vertically)
            #   ---------------     ---------------
            #   |     --------|   --|-------------|            ---------
            #   |     | t2    |   | |       t2    |            |  t2    |
            #   |     |       |   |_|_____________|            |        |
            #   |     |       |     |             |        ----|--------|
            #   |  t1 |       |     |     t1      |       |    | t1     |
            #   |     |_______|     |             |       |    |________|
            #   |_____________|     |_____________|       |_____________|
            return False

        # If code is reached here,
        # they have the same max_x, max_y:
        # Meaning they "end" the same
        elif self.min_y < tb2.min_y:
            # self should be before because it "starts" before
            # The top edge is before t2's top edge
            #
            #     (included)                (a bit outside vertically)
            #   ---------------
            #   |     --------|               ---------
            #   |     | t2    |               |  t1    |
            #   |     |       |               |        |
            #   |     |       |           ----|--------|
            #   |  t1 |       |          |    | t2     |
            #   |     |       |          |    |        |
            #   |_____|_______|          |____|________|
            return True
        elif tb2.min_y < self.min_y:
            # t2 should be before because it "starts" before
            # The top edge is before self's top edge
            #
            #     (included)                (a bit outside vertically)
            #   ---------------
            #   |     --------|               ---------
            #   |     | t1    |               |  t2    |
            #   |     |       |               |        |
            #   |     |       |           ----|--------|
            #   |  t2 |       |          |    | t1     |
            #   |     |       |          |    |        |
            #   |_____|_______|          |____|________|
            return False

        # If code is reached here,
        # they have the same max_x, max_y, min_y:
        # Meaning, only the left edge is different
        elif self.min_x < tb2.min_x:
            # self should be before because it "starts" before
            # The left edge is before t2's left edge
            #
            #   ---------------
            #   |     |       |
            #   |     | t2    |
            #   |     |       |
            #   |     |       |
            #   |  t1 |       |
            #   |     |       |
            #   |_____|_______|
            return True
        elif tb2.min_x < self.min_x:
            # t2 should be before because it "starts" before
            # The left edge is before self's left edge
            #
            #   ---------------
            #   |     |       |
            #   |     | t1    |
            #   |     |       |
            #   |     |       |
            #   |  t2 |       |
            #   |     |       |
            #   |_____|_______|
            return False
        elif self.min_x == tb2.min_x:
            # This should be the last case
            # If code is reached here:
            # min_x, max_x, min_y, max_y are the same for both textboxes.
            # This means they have the same rectangle, only the texts might
            # differ

            # We just return True by default because it's not clear if you can
            # use another metric to decide the order.
            return True
        else:
            raise ValueError("Error when comparing text boxes. Should not happen.")


def merge_text_boxes(text_boxes: List[TextBox]) -> TextBox:
    """
    Given a group of text_boxes, will merge into one textbox in order
    from left to right, top to bottom.
    """
    sorted_by_coordinates = sorted(text_boxes)

    min_x = min(sorted_by_coordinates, key=lambda text_box: text_box.p11[0]).p11[0]
    min_y = min(sorted_by_coordinates, key=lambda text_box: text_box.p11[1]).p11[1]

    max_x = max(sorted_by_coordinates, key=lambda text_box: text_box.p22[0]).p22[0]
    max_y = max(sorted_by_coordinates, key=lambda text_box: text_box.p22[1]).p22[1]

    new_text_box = TextBox(
        id=str(uuid.uuid4()),
        p11=(min_x, min_y),
        p22=(max_x, max_y),
        content=" ".join([x.content for x in sorted_by_coordinates]),
    )
    return new_text_box


def are_text_boxes_neighbours(
    text_box1: TextBox, text_box2: TextBox, threshold: float
) -> bool:
    distance = calculate_distance(text_box1.box_raw, text_box2.box_raw)
    return distance <= threshold


def calculate_distance(boxA: RawRectangle, boxB: RawRectangle) -> float:
    """
    Calculate the distance between two bounding boxes.
    Bounding box format: (x1, y1, x2, y2)
    Returns: distance (0 if boxes overlap or touch)
    """
    p11_A = (boxA[0], boxA[1])
    p22_A = (boxA[2], boxA[3])

    p11_B = (boxB[0], boxB[1])
    p22_B = (boxB[2], boxB[3])

    x_distance: float
    y_distance: float

    # Check overlap or touch in x-axis

    if p11_A[0] <= p22_B[0] and p11_B[0] <= p22_A[0]:
        # Then overlap
        # p11_A
        #   ----------------------------
        #   |                           |
        #   |           tA              |
        #   |___________________________| p22_A
        #           p11_B ----------------------------
        #                 |                           |
        #                 |           tB              |
        #                 |___________________________| p22_B
        x_distance = 0
    else:
        # Then overlap
        # p11_A
        #   ----------------------------
        #   |                           |
        #   |           tA              |
        #   |___________________________| p22_A
        #                               |-------d1-------|
        #                                           p11_B ----------------------------
        #                                                 |                           |
        #                                                 |           tB              |
        #                                                 |___________________________| p22_B
        #   |-----------------------------------d2------------------------------------|
        d1 = abs(p11_B[0] - p22_A[0])
        d2 = abs(p11_A[0] - p22_B[0])  # Becomes shorter if tB is placed before tA
        x_distance = min(d1, d2)

    # Check overlap or touch in y-axis
    if p11_A[1] <= p22_B[1] and p11_B[1] <= p22_A[1]:
        # p11_A
        #   ----------------------------
        #   |                           |   p11_B
        #   |           tA              |   ----------------------------
        #   |___________________________|   |                           |
        #                           p22_A   |           tB              |
        #                                   |___________________________| p22_B
        y_distance = 0
    else:
        # p11_A
        #   -------------------------------------------------------------------------|
        #   |                           |                                            |
        #   |           tA              |                                            |
        #   |___________________________| ___                                        |
        #                             p22_A  |                                      d2
        #                                   d1   p11_B                               |
        #                                    |-------------------------------        |
        #                                       |                           |        |
        #                                       |           tB              |        |
        #                                       |___________________________| p22_B  |

        d1 = abs(p11_B[1] - p22_A[1])
        d2 = abs(p11_A[1] - p22_B[1])
        y_distance = min(d1, d2)  # Becomes shorter if tB is placed before tA

    # Calculate the total distance
    distance = (x_distance**2 + y_distance**2) ** 0.5

    return distance


def min_distance(text_boxes: List[TextBox]) -> float:
    """
    Calculate the smallest distance between test_boxes.

    O(n²) operation.
    """
    min_d = None
    if len(text_boxes) <= 1:
        raise ValueError("text_boxes must have length >= 2")
    for i, tb1 in enumerate(text_boxes):
        for j in range(i + 1, len(text_boxes)):
            tb2 = text_boxes[j]

            d = calculate_distance(tb1.box_raw, tb2.box_raw)
            if min_d is None:
                min_d = d
            else:
                min_d = min(min_d, d)

    if min_d is None:
        raise ValueError("min_d is None. Should not happen")
    return min_d
