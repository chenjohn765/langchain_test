from typing import List

from src.ocr.text_boxes import (
    TextBox,
    are_text_boxes_neighbours,
    create_new_group_memo,
    merge_text_boxes,
    min_distance
)


def greedy_merge_text_boxes(
    text_boxes: List[TextBox], threshold: float
) -> List[TextBox]:
    """
    Idea of the strategy:

    For each text_box, check other text_boxes if they are neighbour. If
    a neighbour is found, group with him as merge candidate. If someone
    from the group encounters a new neighbour, bring the neighbour to the group.

    Once everyone has visited the others to group with him or not, for every
    group formed, merge the text_boxes within this group, based on the ordering
    defined in the TextBox class.

    This is O(n²) operation.
    """
    group_memo = create_new_group_memo()

    # For each element, check if it must group with someone else
    for i, first_text_box in enumerate(text_boxes):
        for j in range(i + 1, len(text_boxes)):
            second_text_box = text_boxes[j]

            if not are_text_boxes_neighbours(
                first_text_box, second_text_box, threshold=threshold
            ):
                continue

            # else
            first_text_box.group_with(second_text_box, group_memo)

    new_text_boxes: List[TextBox] = []
    merge_ids = group_memo["merge_ids"]
    groups = group_memo["groups"]

    # Add first boxes that were not tagged as merge candidates
    for text_box in text_boxes:
        text_box_merge_id = merge_ids.get(text_box.id)
        if not text_box_merge_id:
            new_text_boxes.append(text_box)

    # Merge those that were tagged as merge candidates
    for group in groups.values():
        merged_box = merge_text_boxes(group)
        new_text_boxes.append(merged_box)

    return new_text_boxes


def greedy_grow(text_boxes: List[TextBox]) -> List[TextBox]:
    """
    Idea of the strategy

    1. Uses the greedy_merge_text_boxes strategy starting from threshold = 10
    to merge all small text_boxes that are usually sentences split for some reason.
    ( O(n²) )

    After this first step you should be left with text_boxes well separated.

    2. Then grow threshold to the minimum distance separating two text_boxes.
        ( O(n²) )

    3. And apply the greedy_merge_text_boxes again. This should only merge a few of
    them, and using the minimum thresold will help in text_boxes not looking too far
    for their neighbour (ex, a title that is going to look for another title on the
    right, instead of looking just for the paragraph below him)
        ( O(n²) )

    4. Repeat process 2. & 3. until 1 text_box is left.
        O(n) loop


    Final complexity: O(n^3)
    Not ideal.

    Also, this algorithm can still have mishapes if the document's structure is
    complex.
    """
    threshold: float = 10
    while len(text_boxes) > 1:
        text_boxes = greedy_merge_text_boxes(text_boxes, threshold)

        if len(text_boxes) <= 1:
            break

        min_d = min_distance(text_boxes)

        threshold = min_d
        # NOTE: you can also change this value to threshold = alpha * min_d
        # where you determine alpha yourself.
        # Putting threshold as always the min_distance will usually make it that
        # we only merge two elements per loop.
        # By putting a slight extra margin with alpha, you might get more candidates
        # for merging, that were not included initially:
        # e.g:
        #   ----------------------------    ----------------------------
        #   |                           |   |                           |
        #   |           t1              |   |           t2              |
        #   |___________________________|   |___________________________|
        #
        #   ----------------------------
        #   |                           |
        #   |           t3              |
        #   |___________________________|
        #
        #  Let's say dist(t1, t2) = 10,  and dist(t1, t3) = 12
        #  By adding and extra margin, you would include both t2 & t3 at the
        #  same time, instead of just t2.
        #  NOTE: Be careful, because a larger margin will have more impact
        #  min_distance if big.

    return text_boxes
