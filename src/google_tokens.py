"""Running this script will prompt you to a google login. It will then print your tokens in your console."""

import logging
import os

from google_auth_oauthlib.flow import InstalledAppFlow


logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

SCOPES = ["https://www.googleapis.com/auth/drive.readonly"]

flow = InstalledAppFlow.from_client_secrets_file(
    os.path.join(".credentials", ".desktop_app_credentials.json"), SCOPES
)
creds = flow.run_local_server(port=0)
logger.info(f"Your credentials: {creds.to_json()}")
