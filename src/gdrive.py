# flake8: noqa: E501

# Use the advanced version.
from src.gdrive_loader.loader import FixedGoogleDriveLoaderEnhanced
import logging


logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


document_id = "1qmTTThWXw6RUS1qRvzlcKanjXklcgrfVNRWPz6bQ37U"
# alivia_folder_id = "1_0IJ64BUvRo1vkcFHixmQ5__Op_qOGcF"
alivia_folder_id = "0AOAIi2KIB0NjUk9PVA"
alivia_folder_id = "14vJovJt6B9misitHb8kWDXBmwkhFden8"
# Folder of different types of files
alivia_folder_id = "1LzWZgkl_ZCxlaIgyETwDcKgAVKgNLXdR"
folder_id = "1wzIteYYO9cFpq_NYM6Q2nhN9M9ZnqpDq"
# folder_id = "1okRzcb_G20xReWXLefQCXYpxPovQqhVx"

SCOPES = ["https://www.googleapis.com/auth/drive.readonly"]


# ================== Observations =================================== #
# GoogleDriveLoaderEnhanced loads credentials based on either desktop_app credentials OR service account
# But it will not work with web_app_credentials.
# The loader also requires `credentials_path`, evne if token_path is already defined.

# GoogleDriveLoader accepts web_app_credentials but will not work because of redirect_uri_mismatch
# That is because, the redirect_uri's port is random and therefore cannot be defined in Google's authorized redirect urls
# However it can work with just a token_path=".credentals/token.json"

# It is not possible to use Web app credentials for this GoogleDriveLoader
# But it is possible to use a desktop app credentials to login & fetch the tokens
# Then it is possible to replace the `client_id` and `client_secret` of the desktop json file
# With those of the web app json file, and it will still work.

loader = FixedGoogleDriveLoaderEnhanced(
    # ===================== Using Service account ====================================
    # You need to share the Gdrive files with the email associated with the service account
    # created.
    # service_account_key=".credentials/.service_account_keys.json",
    # service_account_key=".credentials/.poly_test_credentials.json",
    # ===================== Using Oauth2 client id Credentials =====================================
    # Calling credentials_path will trigger a popup window to login.
    # This will then store a token.json file in ~/.credentials/token.json
    # which it will use for the next times
    # It will renew the file if the token expires.
    # Unlike service account, you don't need to share files. Taht is because those credentials
    # are already linked to your Gmail address, and therefore have the same policies ?
    # Web app credentials do not work if code is called from script launched without an API ?
    # credentials_path=".credentials/.web_app_credentials.json",
    # Desktop credentials work if code is called from script launched by hand.
    # credentials_path=".credentials/.web_app_credentials.json",
    # credentials_path=".credentials/.whatever.json",
    credentials_path=".credentials/.whatever.json",
    folder_id=alivia_folder_id,
    token_path=".credentials/token.json",
    # token_path=".credentials/token.json",
    # folder_id=folder_id,
    # folder_id="14vJovJt6B9misitHb8kWDXBmwkhFden8",
    # document_ids=["1f7hUSkMtRGts40YJCYlFn51oLl8pa5v-7_GQstjqf6c"],
    # document_ids=[document_id],
    # Optional: configure whether to recursively fetch files from subfolders. Defaults to False.
    recursive=True,
)
# documents loading
documents = loader.load(includeItemsFromAllDrives=True)

print(documents)
print([(x.metadata["mimeType"], x.metadata["name"]) for x in documents])
