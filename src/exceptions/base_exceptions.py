from typing import Any, Dict, Optional

from fastapi.exceptions import HTTPException


class CustomBaseException(HTTPException):
    def __init__(
        self,
        status_code: int,
        detail: Any,
        headers: Optional[Dict[str, str]] = None,
    ) -> None:
        super().__init__(
            status_code=status_code,
            detail={
                "message": detail,
                # the name of the class
                "error_class": type(self).__name__,
            },
            headers=headers,
        )
