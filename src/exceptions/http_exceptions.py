from src.exceptions.base_exceptions import CustomBaseException


# ================== Ingest ==================================
class AzureIngestException(CustomBaseException):
    def __init__(self) -> None:
        super().__init__(
            status_code=422,
            detail="Ingestion of document into Azure failed.",
        )


# ================== Gdrive ==================================
class GdriveConnectionFailed(CustomBaseException):
    def __init__(self) -> None:
        super().__init__(
            status_code=422,
            detail="Could not connect to google drive.",
        )


class EmptyOrUnknownGooleDriveFolderException(CustomBaseException):
    def __init__(self) -> None:
        super().__init__(
            status_code=404,
            detail="The google drive folder provided is empty or does not exist.",
        )


class UnknownGooleDriveFolderException(CustomBaseException):
    def __init__(self) -> None:
        super().__init__(
            status_code=404,
            detail="The google drive folder provided does not exist.",
        )


class UnexpectedLoadingGoogleDriveException(CustomBaseException):
    def __init__(self) -> None:
        super().__init__(
            status_code=500,
            detail="Unexpected error when loading google drive",
        )
