from langchain.llms import OpenAI
from src.settings import settings
from langchain.document_loaders import TextLoader, DirectoryLoader
from langchain.indexes import VectorstoreIndexCreator

llm = OpenAI(openai_api_key=settings.OPENAI_API_KEY)

loader = DirectoryLoader("src/documents", glob="*.txt")
# Result of loader.load():
# [
# Document(page_content='I have one brother\n\nI weight 64kg...',
#   metadata={'source': 'src/documents/presentation.txt'}),
# Document(page_content='Conversation between two people:\n\nsoloist...',
#   metadata={'source': 'src/documents/conversation.txt'})]
index = VectorstoreIndexCreator().from_loaders([loader])
vectorstore = index.vectorstore
doc_retriver = vectorstore.as_retriever()
relevant_docs = doc_retriver.get_relevant_documents("who is the strongest pokemont ?")


print(f"{relevant_docs=}")
# query = "Who am I ? Please give the most details as possible"
# query = "Please summarize the conversation between the two people in my document"
# result = index.query(query)
context1 = """
Ceci est un discussion entre plusieurs personnes:

Tom: Je pars du nombre 1.
Jean: Ajoute encore 1 à ce nombre.
Luc: Ajoute 2.
Paul: Ajoute 5.
Quel est le résultat ?
"""
query1 = f"""En tenant compte de ce context qui était une discussion précédente: {context1}
    Qu'est-ce que Paul a dit ?
    """

context2 = """
    Ceci est un discussion entre plusieurs personnes:
    Tom: Quand a commencé la Première guerre mondiale ?
    Jean: Je ne sais pas.
"""
query2 = f"""En tenant compte de ce context qui était une discussion précédente: {context2}
    Est-ce que toi tu sais ?
    """


# context2 = """
# I have one brother
# I weight 64kg
# I am 175cm tall
# I play genshin impact, honkai impact and honkai star rail
# """
# query2 = f"Given the context {context2}, tell me who I am ?"
# result = llm.predict(query2)

# query2 = "Qui était le leader du parti nazi lors de la seconde guerre mondiale ? "
query2 = "Who is the strongest pokemon ? Please provide the name of the source that allowed you to answer at the end of your message"
result = index.query(query2)
# print(result)
