
from typing import List

from langchain.docstore.document import Document
from langchain.document_loaders import ConfluenceLoader, GoogleDriveLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter
# Use the advanced version.
from langchain_googledrive.document_loaders import GoogleDriveLoader as GoogleDriveLoaderEnhanced
from contextlib import contextmanager
import json
from datetime import datetime, timedelta
import os


@contextmanager
def create_token_file(access_token: str):
    # Code to acquire resource, e.g.:
    try:
        expiry_date = datetime.now() + timedelta(hours=2)
        path = ".credentials/token.json"
        with open(path, "w") as f:
            json.dump({
                "token": access_token,
                "refresh_token": "toto",
                "token_uri": "https://oauth2.googleapis.com/token",
                "client_id": "1049489025546-62gokuirah3q10jggul59fd7dio2g8nl.apps.googleusercontent.com",
                "client_secret": "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",
                "scopes": ["https://www.googleapis.com/auth/drive.readonly"],
                "expiry": expiry_date.isoformat()
                }, f)

        yield path
    finally:
        # Code to release resource, e.g.:
        os.remove(path)
        

document_id = "1qmTTThWXw6RUS1qRvzlcKanjXklcgrfVNRWPz6bQ37U"
folder_id = "1wzIteYYO9cFpq_NYM6Q2nhN9M9ZnqpDq"


token = "ya29.a0AfB_byBF2_1S92XrSRO2X4h98lel7ZoZfFrXU_cEMVt6PN462X4bq5frN6DXK1ezQyzJJfA927Ld4nFH9DFKmppZS-ZW_Vo6v9_hS9HXIAlR1dkyGf8-O0qlY1m67yMc8VD7px0oNAW11f6axfeXdw94LuEqflzmKidkaCgYKASwSARASFQGOcNnCwFPzesGojT5QUke_AtTXnw0171"
with create_token_file(token) as token_path:
    loader = GoogleDriveLoaderEnhanced(
        # ===================== Using Service account ====================================
        # You need to share the Gdrive files with the email associated with the service account
        # created.
        # service_account_key=".credentials/.service_account_keys.json",
        # service_account_key=".credentials/.poly_test_credentials.json",

        # ===================== Using Oauth2 client id Credentials =====================================
        # Calling credentials_path will trigger a popup window to login.
        # This will then store a token.json file in ~/.credentials/token.json
        # which it will use for the next times
        # It will renew the file if the token expires.
        # Unlike service account, you don't need to share files. Taht is because those credentials
        # are already linked to your Gmail address, and therefore have the same policies ?

        # Web app credentials do not work if code is called from script launched without an API ?
        # credentials_path=".credentials/.web_app_credentials.json",

        # Desktop credentials work if code is called from script launched by hand.
        credentials_path=".credentials/.desktop_app_credentials.json",
        token_path=token_path,
        folder_id=folder_id,

        # folder_id="14vJovJt6B9misitHb8kWDXBmwkhFden8",
        # document_ids=["1f7hUSkMtRGts40YJCYlFn51oLl8pa5v-7_GQstjqf6c"],

        # document_ids=[document_id],
        # Optional: configure whether to recursively fetch files from subfolders. Defaults to False.
        recursive=True,
    )
    # documents loading
    documents = loader.load()

    print(documents)
