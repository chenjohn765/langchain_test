import time
from contextlib import contextmanager
from logging import Logger
from typing import Generator


@contextmanager
def timer(logger: Logger, start_message: str) -> Generator[None, None, None]:
    """
    Example of usage:

    ==================================================================
    import logging

    logger = logging.getLogger(__name__)
    logging.basicConfig(level=logging.INFO)

    with timer(logger, "Fetching data ..."):
        data = fetch(...)

    # Will log
    > INFO:__main__:Fetching data...
    > INFO:__main__:Finished fetching data. Took xx s.
    ===================================================================

    NOTE: this is a slower than if you wrote the script directly.

    # 3.1 µs
    with timer(logger, "Calculating stuff..."):
    x = 1 + 1

    # 0.7 µs
    logger.info("Calculating stuff...")
    start = time.time()
    x = 1+1
    end = time.time()
    logger.info(f"Finished calculating stuff. Took {end - start}s")
    """
    logger.info(start_message)
    start = time.time()
    yield

    # cleanup
    end = time.time()
    end_message = (
        "Finished "
        + start_message[0].lower()
        + start_message[1:].replace("...", ".")
        + " "
        + f"Took {end - start}s"
    )
    logger.info(end_message)
