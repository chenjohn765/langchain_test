SHELL := /bin/bash

.PHONY: init
init:
	poetry install

.PHONY: requirements
requirements:
	poetry export -o requirements.txt


.PHONY: server
server:
	set -a && source .env && set +a && uvicorn src.main:app --reload


# ================ TESTS ============================
.PHONY: test
test:
	set -a && source .env.test && set +a && pytest tests/ $(c)

# ================ QUALITY ============================
.PHONY: quality
quality:
	flake8 .
	isort . --check --diff
	mypy .

.PHONY: format
format:
	black .
	isort .
