from unittest.mock import Mock, patch

from langchain.docstore.document import Document

from src.gdrive_loader.pdf_loader import CustomPDFLoader


class TestCustomPDFLoader:
    def test_custom_pdf_loader_should_recognize_if_pdf_is_extractable(self) -> None:
        assert CustomPDFLoader(
            file_path="files/pdf_files/tricky_text.pdf"
        ).is_pdf_text_extractable
        assert not CustomPDFLoader(
            file_path="files/pdf_files/only_image.pdf"
        ).is_pdf_text_extractable

    def test_custom_pdf_loader_should_use_custom_ocr_method_if_pdf_not_extractable(
        self,
    ) -> None:
        pdf_loader = CustomPDFLoader(file_path="files/pdf_files/multi_page.pdf")

        with patch(
            "src.gdrive_loader.pdf_loader.CustomPDFLoader.read_single_image",
            side_effect=[
                Document(
                    page_content=content,
                    metadata={
                        "gdriveId": "gdrive_Id",
                        "title": "multi_page.pdf",
                        "source": "gdrive-url",
                    },
                )
                for content in ["page1", "page2", "page3"]
            ],
        ):
            documents = pdf_loader.load()
            assert len(documents) == 1
            assert documents[0] == Document(
                page_content="page1\n\npage2\n\npage3",
                metadata={
                    "gdriveId": "gdrive_Id",
                    "title": "multi_page.pdf",
                    "source": "gdrive-url",
                },
            )

    def test_custom_pdf_loader_should_use_original_method_if_pdf_extractable(
        self,
    ) -> None:
        pdf_loader = CustomPDFLoader(file_path="files/pdf_files/tricky_text.pdf")
        mock = Mock()
        with patch(
            "src.gdrive_loader.pdf_loader.UnstructuredPDFLoader.load",
            return_value=[mock],
        ):
            assert pdf_loader.load() == [mock]
