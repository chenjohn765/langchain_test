# We ignore the line too long for this file as this is for promts
# It's expected to have very long lines.
# flake8: noqa: E501


from src.ocr.merge_text_boxes_strategies import (
    greedy_grow,
    greedy_merge_text_boxes
)
from src.ocr.text_boxes import TextBox


class TestGreedyMergeTextBoxes:
    def test_greedy_merge_text_boxes_should_merge_in_correct_order_if_neighbour(
        self,
    ) -> None:
        # Case: In usual left to right reading, order would be t1 - t3 - t2 - t4
        # However, in real situations, such block structures follow the below order
        #
        #   ----------------------------                ----------------------------
        #   |                           |               |                           |
        #   |           t1              |               |           t3              |
        #   |___________________________|               |___________________________|
        #   ----------------------------                ----------------------------
        #   |                           |               |                           |
        #   |           t2              |               |           t4              |
        #   |___________________________|               |___________________________|

        t1 = TextBox(content="t1", p11=(1, 1), p22=(10, 10))
        t2 = TextBox(content="t2", p11=(1, 11), p22=(10, 21))

        t3 = TextBox(content="t3", p11=(50, 1), p22=(60, 10))
        t4 = TextBox(content="t4", p11=(50, 11), p22=(60, 21))

        result = greedy_merge_text_boxes([t2, t3, t1, t4], 10)
        result = sorted(result)
        assert len(result) == 2
        assert result[0].content == "t1 t2"
        assert result[1].content == "t3 t4"

    def test_greedy_grow_should_merge_text_boxes_until_one_left(self) -> None:
        # Case: In usual left to right reading, order would be t1 - t3 - t2 - t4
        # However, in real situations, such block structures follow the below order
        #
        #                          x=5
        #                           ----------------------------
        #                           |                           |
        #                           |           t0              |
        #                           |___________________________| x=55 y=10
        #
        #   x=1, y=30                x=10              x=50, y=30
        #   ----------------------------                ----------------------------
        #   |                           |               |                           |
        #   |           t1              |               |           t3              |
        #   |___________________________|               |___________________________|x=60, y=40
        #   ----------------------------                ----------------------------
        #   |                           |               |                           |
        #   |           t2              |               |           t4              |
        #   |___________________________|               |___________________________|x=60, y=50
        #

        t0 = TextBox(content="t0", p11=(5, 0), p22=(55, 10))
        t1 = TextBox(content="t1", p11=(1, 30), p22=(10, 40))
        t2 = TextBox(content="t2", p11=(1, 40), p22=(10, 50))

        t3 = TextBox(content="t3", p11=(50, 30), p22=(60, 40))
        t4 = TextBox(content="t4", p11=(50, 40), p22=(60, 50))

        result = greedy_grow([t2, t3, t1, t4, t0])
        assert len(result) == 1
        assert result[0].content == "t0 t1 t2 t3 t4"
