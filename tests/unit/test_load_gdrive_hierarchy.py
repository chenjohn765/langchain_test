from typing import Dict
from unittest.mock import Mock

import pytest
from googleapiclient.errors import HttpError as GoogleHttpError

from src.exceptions.http_exceptions import (
    UnexpectedLoadingGoogleDriveException,
    UnknownGooleDriveFolderException
)
from src.gdrive_loader.load_hierarchy import list_subfolders_hierarchy


def mock_google_api_list_call(
    q: str, fields: str, includeItemsFromAllDrives: bool, supportsAllDrives: bool
) -> Mock:
    assert fields == "files(id, mimeType)"
    assert supportsAllDrives is True
    assert includeItemsFromAllDrives is True

    # Let's assume a hierarchy
    #               folder_A
    #               |
    #    -----------------------
    #   |           |
    #  file_a       folder_B
    #               |
    #           ---------------
    #           |              |
    #          file_b          |
    #                       folder_C
    #                          |
    #                       file_c
    def execute() -> Dict:
        if q == "'folder_A' in parents":
            return {
                "files": [
                    {
                        "id": "file_a",
                        "mimeType": "application/vnd.google-apps.document",
                    },
                    {
                        "id": "folder_B",
                        "mimeType": "application/vnd.google-apps.folder",
                    },
                ]
            }
        elif q == "'folder_B' in parents":
            return {
                "files": [
                    {
                        "id": "file_b",
                        "mimeType": "application/vnd.google-apps.document",
                    },
                    {
                        "id": "folder_C",
                        "mimeType": "application/vnd.google-apps.folder",
                    },
                ]
            }
        elif q == "'folder_C' in parents":
            return {
                "files": [
                    {
                        "id": "file_c",
                        "mimeType": "application/vnd.google-apps.document",
                    },
                ]
            }
        elif q == "'unknown' in parents":
            raise GoogleHttpError(
                resp=Mock(**{"status": 404, "reason": "reason"}), content=b"toto"
            )
        else:
            raise GoogleHttpError(
                resp=Mock(**{"status": 500, "reason": "unexpected_reason"}),
                content=b"toto",
            )

    return Mock(**{"execute": execute})


mock_google_service = Mock(**{"files.return_value.list": mock_google_api_list_call})


class TestListSubfoldersHierarchy:
    def test_list_subfolders_hierarchy_should_return_correct_hierarchy(self) -> None:
        assert list_subfolders_hierarchy(mock_google_service, "folder_A") == [
            {
                "id": "file_a",
                "mimeType": "application/vnd.google-apps.document",
                "path_id": "folder_A/file_a",
            },
            {
                "id": "folder_B",
                "mimeType": "application/vnd.google-apps.folder",
                "path_id": "folder_A/folder_B",
            },
            {
                "id": "file_b",
                "mimeType": "application/vnd.google-apps.document",
                "path_id": "folder_A/folder_B/file_b",
            },
            {
                "id": "folder_C",
                "mimeType": "application/vnd.google-apps.folder",
                "path_id": "folder_A/folder_B/folder_C",
            },
            {
                "id": "file_c",
                "mimeType": "application/vnd.google-apps.document",
                "path_id": "folder_A/folder_B/folder_C/file_c",
            },
        ]

    def test_list_subfolders_hierarchy_should_prefix_if_parent_path_provided(
        self,
    ) -> None:
        assert list_subfolders_hierarchy(
            mock_google_service, "folder_C", parent_path_id="folder_A/folder_B"
        ) == [
            {
                "id": "file_c",
                "mimeType": "application/vnd.google-apps.document",
                "path_id": "folder_A/folder_B/folder_C/file_c",
            },
        ]

    def test_get_parent_path_should_raise_error_if_folder_not_exist(
        self,
    ) -> None:
        with pytest.raises(UnknownGooleDriveFolderException):
            list_subfolders_hierarchy(mock_google_service, "unknown")

    def test_get_parent_path_should_raise_error_if_unexpected_google_error(
        self,
    ) -> None:
        with pytest.raises(UnexpectedLoadingGoogleDriveException):
            list_subfolders_hierarchy(mock_google_service, "raise_unexpected_error")
