from unittest.mock import Mock, patch

import pytest

from src.exceptions.http_exceptions import AzureVisionReadException
from src.gdrive_loader.azure_vision import AzureResult, azure_read_img
from src.ocr.text_boxes import TextBox

mock_azure_response: AzureResult = {
    "readResult": {
        "content": "",
        "stringIndexType": "TextElements",
        "pages": [
            {
                "height": 686.0,
                "width": 1220.0,
                "angle": 0.0,
                "pageNumber": 1,
                "words": [],
                "lines": [
                    {
                        "content": "INTRODUCTION À CHATGPT",
                        "boundingBox": [
                            21.0,
                            55.0,
                            443.0,
                            54.0,
                            443.0,
                            91.0,
                            21.0,
                            92.0,
                        ],
                    },
                    {
                        "content": "ET OPENAI",
                        "boundingBox": [
                            23.0,
                            98.0,
                            184.0,
                            98.0,
                            184.0,
                            124.0,
                            23.0,
                            124.0,
                        ],
                    },
                ],
            }
        ],
    }
}


class TestAzureVision:
    def test_azure_read_img_should_raise_if_unexpcted_error(self) -> None:
        mock_response = Mock()
        mock_response.status_code = 500
        mock_response.json.return_value = "errorMessage"
        with patch(
            "requests.post",
            return_value=mock_response,
        ), pytest.raises(AzureVisionReadException):
            azure_read_img("files/img_files/img_complex_1.png")

    def test_azure_read_img_should_call_Azure_and_return_text_boxes(self) -> None:
        mock_response = Mock()
        mock_response.status_code = 200
        mock_response.json.return_value = mock_azure_response
        with patch(
            "requests.post",
            return_value=mock_response,
        ) as mock_post:
            text_boxes = azure_read_img("files/img_files/img_complex_1.png")

        # Then
        bbox1 = mock_azure_response["readResult"]["pages"][0]["lines"][0]["boundingBox"]
        bbox2 = mock_azure_response["readResult"]["pages"][0]["lines"][1]["boundingBox"]

        assert text_boxes == [
            TextBox(
                id=text_boxes[0].id,
                content="INTRODUCTION À CHATGPT",
                p11=(bbox1[0], bbox1[1]),
                p22=(bbox1[4], bbox1[5]),
            ),
            TextBox(
                id=text_boxes[1].id,
                content="ET OPENAI",
                p11=(bbox2[0], bbox2[1]),
                p22=(bbox2[4], bbox2[5]),
            ),
        ]

        mock_post.assert_called_once()
