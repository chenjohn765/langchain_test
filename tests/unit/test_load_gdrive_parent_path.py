from typing import Dict
from unittest.mock import Mock

import pytest
from googleapiclient.errors import HttpError as GoogleHttpError

from src.exceptions.http_exceptions import (
    UnexpectedLoadingGoogleDriveException,
    UnknownGooleDriveFolderException
)
from src.gdrive_loader.load_hierarchy import get_parent_path


def mock_google_api_get_call(fileId: str, fields: str, supportsAllDrives: bool) -> Mock:
    assert fields == "parents"
    assert supportsAllDrives is True

    def execute() -> Dict:
        # Let's imagine a hierarchy like that:
        # A -> B -> C -> D-> E
        folder_id = fileId
        if folder_id == "unknown":
            raise GoogleHttpError(
                resp=Mock(**{"status": 404, "reason": "reason"}), content=b"toto"
            )
        if folder_id == "raise_unexpected_error":
            raise GoogleHttpError(
                resp=Mock(**{"status": 500, "reason": "unexpected_reason"}),
                content=b"toto",
            )

        hierarchy = [None, "A", "B", "C", "D", "E"]
        parent = hierarchy[hierarchy.index(folder_id) - 1]
        #  that's how the google service API returns the answer
        if not parent:
            return {}
        return {"parents": [parent]}

    return Mock(**{"execute": execute})


mock_google_service = Mock(**{"files.return_value.get": mock_google_api_get_call})


class TestGetParentFolderIds:
    def test_get_parent_path_should_return_correct_path(
        self,
    ) -> None:
        # Then
        assert get_parent_path(mock_google_service, "E") == "A/B/C/D"
        assert get_parent_path(mock_google_service, "D") == "A/B/C"
        assert get_parent_path(mock_google_service, "A") == ""

    def test_get_parent_path_should_raise_error_if_folder_not_exist(
        self,
    ) -> None:
        with pytest.raises(UnknownGooleDriveFolderException):
            get_parent_path(mock_google_service, "unknown")

    def test_get_parent_path_should_raise_error_if_unexpected_google_error(
        self,
    ) -> None:
        with pytest.raises(UnexpectedLoadingGoogleDriveException):
            get_parent_path(mock_google_service, "raise_unexpected_error")
