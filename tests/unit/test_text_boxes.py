# We ignore the line too long for this file as this is for promts
# It's expected to have very long lines.
# flake8: noqa: E501

from src.ocr.text_boxes import (
    TextBox,
    calculate_distance,
    create_new_group_memo,
    merge_text_boxes,
)

# We assume that a rectangle is formed by 4 points:
#  p11 ------------------ p12
#   |                       |
#  p21 -------------------p22
#
#  However, it only needs p11 & p22 to be defined.
#
# A point such as p11 is defined by his tuple (x, y)
#
# Axis are the following
# 0 ----> x
# |
# |
# y


class TestTextBox:
    def test_text_box_should_be_sortable(self) -> None:
        # Classic case: on top of each other
        #   ----------------------------
        #   |                           |
        #   |           t1              |
        #   |___________________________|
        #   ----------------------------
        #   |                           |
        #   |           t2              |
        #   |___________________________|

        assert TextBox(content="t1", p11=(1, 1), p22=(10, 10)) < TextBox(
            content="t2", p11=(1, 11), p22=(10, 21)
        )
        # Classic case: on top & at the left
        #   ----------------------------
        #   |                           |
        #   |           t1              |
        #   |___________________________|
        #                                   ----------------------------
        #                                   |                           |
        #                                   |           t2              |
        #                                   |___________________________|

        assert TextBox(content="t1", p11=(1, 1), p22=(10, 10)) < TextBox(
            content="t2", p11=(1 + 10, 11), p22=(10 + 10, 21)
        )
        # Classic case: same line, on the left
        #   ----------------------------    ----------------------------
        #   |                           |   |                           |
        #   |           t1              |   |           t2              |
        #   |___________________________|   |___________________________|

        assert TextBox(content="t1", p11=(1, 1), p22=(10, 10)) < TextBox(
            content="t2", p11=(1 + 15, 1), p22=(10 + 15, 10)
        )

        # Tricky case: on top, but at the right
        #
        #                                   ----------------------------
        #                                   |                           |
        #                                   |           t1              |
        #                                   |___________________________|
        #   ----------------------------
        #   |                           |
        #   |           t2              |
        #   |___________________________|

        assert TextBox(content="t1", p11=(1 + 10, 1), p22=(10 + 10, 10)) < TextBox(
            content="t2", p11=(1, 11), p22=(10, 21)
        )

        # Tricky case: positioned slightly lower than t2
        # This is a counter-case to solutions that solve the tricky case
        # just above, by simply relying on the Y-index (which would yield
        # t2 as before t1)
        #                                   ----------------------------
        #   ----------------------------    |                           |
        #   |                           |   |           t2              |
        #   |           t1              |   |___________________________|
        #   |___________________________|

        assert TextBox(content="t1", p11=(1, 1), p22=(10, 10)) < TextBox(
            content="t2", p11=(1 + 15, 0), p22=(10 + 15, 9)
        )

        # Tricky case: positioned close to the same line
        # but t1 should still be considered higher.
        # Based on the center of t1, compared to top edge of t2
        #
        #                                   ----------------------------
        #                                   |                           |
        #   ----------------------------    |           t1              |
        #   |                           |   |___________________________|
        #   |           t2              |
        #   |___________________________|

        t1 = TextBox(content="t1", p11=(1 + 10, 1), p22=(10 + 10, 10))
        assert t1.center == (15.5, 5.5)
        assert t1 < TextBox(
            content="t2",
            # Position slightly below the center
            p11=(1, 6),
            p22=(10, 16),
        )

    def test_merge_text_boxes_should_merge_in_correct_order(self) -> None:
        #                       x= 20       x = 31
        #                       |           |
        #                       ----------   ----------
        #                       |        |   |        |
        #                       |   t1   |   |   t2   |
        #                       |________|   |________|         __ y = 10
        #
        #       x=10    x=11
        #          |   |
        # ----------   ----------      __ y = 11
        # |        |   |        |
        # |   t3   |   |   t4   |
        # |________|   |________|       __ y = 21
        # ----------   ----------       __ y = 21
        # |        |   |        |
        # |   t5   |   |   t6   |
        # |________|   |________|       __ y = 31

        t1 = TextBox(content="t1", p11=(20, 0), p22=(20, 10))
        t2 = TextBox(content="t2", p11=(31, 0), p22=(41, 10))

        t3 = TextBox(content="t3", p11=(0, 11), p22=(10, 21))
        t4 = TextBox(content="t4", p11=(11, 11), p22=(21, 21))

        t5 = TextBox(content="t5", p11=(0, 21), p22=(10, 31))
        t6 = TextBox(content="t6", p11=(11, 21), p22=(21, 31))

        merged_text_box = merge_text_boxes([t1, t2, t3, t4, t5, t6])

        assert merged_text_box.content == "t1 t2 t3 t4 t5 t6"
        assert merged_text_box.p11 == (0, 0)
        assert merged_text_box.p22 == (41, 31)

    def test_merge_text_boxes_should_merge_in_correct_order_tricky_case(self) -> None:
        #                       x= 20       x = 31
        #                       |           |
        #                       ----------   ----------
        #                       |        |   |        |
        #                       |   t1   |   |   t2   |
        #                       |________|   |________|         __ y = 10
        #
        #       x=10    x=11
        #          |   |
        # ----------   ----------               ----------  # In a real reading format,
        # |        |   |        |               |        |  # this box should be, considered as t7,
        # |   t3   |   |   t4   |               |   t4_1 |  # but it is expected that this function
        # |________|   |________|               |________|  # will order it after t4.
        # ----------   ----------                           # As such, we should make sure
        # |        |   |        |                           # this function isn't called in such
        # |   t5   |   |   t6   |                           # a situation
        # |________|   |________|

        # Values are same as test above
        t1 = TextBox(content="t1", p11=(20, 0), p22=(20, 10))
        t2 = TextBox(content="t2", p11=(31, 0), p22=(41, 10))

        t3 = TextBox(content="t3", p11=(0, 11), p22=(10, 21))
        t4 = TextBox(content="t4", p11=(11, 11), p22=(21, 21))

        t5 = TextBox(content="t5", p11=(0, 21), p22=(10, 31))
        t6 = TextBox(content="t6", p11=(11, 21), p22=(21, 31))

        # Add this t4_1 box
        t4_1 = TextBox(content="t4_1", p11=(35, 11), p22=(45, 21))

        merged_text_box = merge_text_boxes([t1, t2, t3, t4, t5, t6, t4_1])

        # Expected order by this function, but in a real case, we don't want that
        assert merged_text_box.content == "t1 t2 t3 t4 t4_1 t5 t6"
        assert merged_text_box.p11 == (0, 0)
        assert merged_text_box.p22 == (45, 31)

    def test_text_boxes_should_be_sortable_tricky_case_max_y(self) -> None:
        #     (included)
        #  x=0           x=10
        #   -------------------- y=0
        #   |    x=5      |
        #   |     --------|----- y=2
        #   |     | t1    |
        #   |     |       |
        #   |     |       |
        #   |  t2 |       |
        #   |     |_______| ____ y=8
        #   |_____________| ____ y=10

        t1 = TextBox(content="t1", p11=(5, 2), p22=(10, 8))
        t2 = TextBox(content="t2", p11=(0, 0), p22=(10, 10))
        assert t1 < t2

        #           (a bit outside)
        #        x=2           x=10
        #         -------------------- y=0
        #         |    x=5      |
        # x=0  ---|-------------|----- y=2
        #      |  |       t1    |
        #      |__|_____________|_____ y=5
        #         |             |
        #         |  t2         |
        #         |             |
        #         |_____________| ____ y=10
        t1 = TextBox(content="t1", p11=(0, 2), p22=(10, 5))
        t2 = TextBox(content="t2", p11=(0, 0), p22=(10, 10))
        assert t1 < t2

        #          (a bit outside vertically)
        #           x=2      x=10
        #            ---------- ------- y=0
        #            |  t1    |
        #            |        |
        #  x=0   ----|--------| ------- y=5
        #       |    | t2     |
        #       |    |________| _______ y=8
        #       |_____________| _______ y=10
        t1 = TextBox(content="t1", p11=(2, 0), p22=(10, 8))
        t2 = TextBox(content="t2", p11=(0, 5), p22=(10, 10))
        assert t1 < t2

    def test_text_boxes_should_be_sortable_tricky_case_min_y(self) -> None:
        #     (included)
        #  x=0           x=10
        #   -------------------- y=0
        #   |    x=5      |
        #   |     --------|----- y=2
        #   |     | t2    |
        #   |     |       |
        #   |     |       |
        #   |  t1 |       |
        #   |     |       |
        #   |_____|_______| ____ y=10
        t1 = TextBox(content="t2", p11=(0, 0), p22=(10, 10))
        t2 = TextBox(content="t1", p11=(5, 2), p22=(10, 10))
        assert t1 < t2

        #          (a bit outside vertically)
        #           x=2      x=10
        #            ---------- ------- y=0
        #            |  t1    |
        #            |        |
        #  x=0   ----|--------| ------- y=5
        #       |    | t2     |
        #       |    |        |
        #       |____|________| _______ y=10
        t1 = TextBox(content="t1", p11=(2, 0), p22=(10, 8))
        t2 = TextBox(content="t2", p11=(0, 5), p22=(10, 10))
        assert t1 < t2

    def test_text_boxes_should_be_sortable_tricky_case_min_x(self) -> None:
        #  x=0  x=4     x=10
        #   --------------- ---- y=0
        #   |     |       |
        #   |     | t2    |
        #   |     |       |
        #   |     |       |
        #   |  t1 |       |
        #   |     |       |
        #   |_____|_______| ____ y=10
        t1 = TextBox(content="t1", p11=(0, 0), p22=(10, 10))
        t2 = TextBox(content="t2", p11=(4, 0), p22=(10, 10))
        assert t1 < t2

    def test_text_boxes_should_be_sortable_tricky_case_same_rectangle(self) -> None:
        # (Same rectangle)
        #  x=0         x=10
        #   -------------- ---- y=0
        #   |            |
        #   |            |
        #   |            |
        #   |    t1      |
        #   |    t2      |
        #   |            |
        #   |____________| ____ y=10
        t1 = TextBox(content="t1", p11=(0, 0), p22=(10, 10))
        t2 = TextBox(content="t2", p11=(0, 0), p22=(10, 10))

        # This ordering is weird, but it's expected behavior
        # given the current implementation
        # who does not clearly define who is before in this case.
        assert t1 < t2
        assert t2 < t1

        # Assert not equal, because the texts might differ
        assert t1 != t2
        # Check that we didn't break reference equality
        assert t1 == t1
        # just run sorted to make sure it does not raise
        sorted([t1, t2])


class TestGroupTextBoxes:
    def test_group_text_boxes_should_group_them_correctly(self) -> None:
        text_boxes = [
            TextBox(
                content="t1",
                id="e0a996d0-0c80-4205-a2c6-c5eaa1b6403c",
                # Points coordinates don't matter in this test
                p11=(1320, 314),
                p22=(1616, 300),
            ),
            TextBox(
                content="t2",
                id="cc9a5376-20a2-4e9e-b3a3-04e01e3e8388",
                p11=(31, 74),
                p22=(236, 118),
            ),
            TextBox(
                content="t3",
                id="c4d3bff4-8277-458c-aefd-951a8492e3b3",
                p11=(258, 70),
                p22=(732, 125),
            ),
            TextBox(
                content="t4",
                id="88e248b8-e370-445b-bb05-9e39566486c0",
                p11=(68, 166),
                p22=(560, 198),
            ),
            TextBox(
                content="t5",
                id="98e248b8-e370-445b-bb05-9e39566486c0",
                p11=(68, 166),
                p22=(560, 198),
            ),
        ]
        group_memo = create_new_group_memo()
        merge_ids = group_memo["merge_ids"]
        for t in text_boxes:
            assert group_memo["merge_ids"].get(t.id) is None

        t1, t2, t3, t4, t5 = text_boxes

        # Grouping like a chain: t3 -> t4 -> t5
        t3.group_with(t4, group_memo)
        t4.group_with(t5, group_memo)

        assert merge_ids[t3.id] == merge_ids[t4.id] == merge_ids[t5.id]
        assert group_memo == {
            "merge_ids": {t3.id: t3.id, t4.id: t3.id, t5.id: t3.id},
            "groups": {t3.id: [t3, t4, t5]},
        }

        # Grouping when text_boxes are selected multiple times
        # Should not change anything
        t3.group_with(t4, group_memo)
        t3.group_with(t5, group_memo)
        t4.group_with(t3, group_memo)
        t4.group_with(t5, group_memo)
        t5.group_with(t3, group_memo)
        t5.group_with(t4, group_memo)

        # This assert is the same as the one just above
        assert merge_ids[t3.id] == merge_ids[t4.id] == merge_ids[t5.id]
        assert group_memo == {
            "merge_ids": {t3.id: t3.id, t4.id: t3.id, t5.id: t3.id},
            "groups": {t3.id: [t3, t4, t5]},
        }

        # Grouping the other part: t1 -> t2
        t1.group_with(t2, group_memo)
        assert group_memo == {
            "merge_ids": {
                t1.id: t1.id,
                t2.id: t1.id,
                t3.id: t3.id,
                t4.id: t3.id,
                t5.id: t3.id,
            },
            "groups": {t1.id: [t1, t2], t3.id: [t3, t4, t5]},
        }

        # Group everything together
        t1.group_with(t3, group_memo)
        assert group_memo == {
            "merge_ids": {
                t1.id: t3.id,
                t2.id: t3.id,
                t3.id: t3.id,
                t4.id: t3.id,
                t5.id: t3.id,
            },
            "groups": {t3.id: [t3, t4, t5, t1, t2]},
        }


class TestCalculateDistance:
    def test_calculate_distance_should_return_0_if_overlapping(self) -> None:
        tA = (0, 0, 20, 20)
        tB = (10, 10, 30, 30)
        assert calculate_distance(tA, tB) == 0

        # Case: tB completely inside tA
        tA = (0, 0, 40, 40)
        tB = (10, 10, 20, 20)
        assert calculate_distance(tA, tB) == 0

    def test_calculate_distance_should_return_0_if_touching(self) -> None:
        # case: on top of each other
        #   ----------------------------
        #   |                           |
        #   |           t1              |
        #   |___________________________|
        #   ----------------------------
        #   |                           |
        #   |           t2              |
        #   |___________________________|
        assert calculate_distance((0, 0, 20, 20), (0, 20, 20, 40)) == 0

        # case: on top of each other, but slightly shifted
        #   ----------------------------
        #   |                           |
        #   |           t1              |
        #   |___________________________|
        #       ----------------------------
        #       |                           |
        #       |           t2              |
        #       |___________________________|
        assert calculate_distance((0, 0, 20, 20), (0 + 5, 20, 20 + 5, 40)) == 0

        # case: same line
        #   ---------------------------- ----------------------------
        #   |                           ||                           |
        #   |           t1              ||           t2              |
        #   |___________________________||___________________________|

        assert calculate_distance((0, 0, 20, 20), (20, 0, 40, 20)) == 0

        # case: next to each other, slightly shifted
        #   ----------------------------
        #   |                           |----------------------------
        #   |           t1              ||                           |
        #   |___________________________||           t2              |
        #                                |___________________________|

        assert calculate_distance((0, 0, 20, 20), (20, 0 + 5, 40, 20 + 5)) == 0

    def test_calculate_distance_should_be_symmetric(self) -> None:
        # case: same line
        #   ----------------------------    ----------------------------
        #   |                           |   |                           |
        #   |           t1              |   |           t2              |
        #   |___________________________|   |___________________________|

        tA = (0, 0, 20, 20)
        tB = (30, 0, 50, 20)
        assert calculate_distance(tA, tB) == calculate_distance(tB, tA) == 10

        # case: on top of each other
        #   ----------------------------
        #   |                           |
        #   |           t1              |
        #   |___________________________|
        #
        #   ----------------------------
        #   |                           |
        #   |           t2              |
        #   |___________________________|
        tA = (0, 0, 20, 20)
        tB = (0, 30, 20, 50)
        assert calculate_distance(tA, tB) == calculate_distance(tB, tA) == 10
