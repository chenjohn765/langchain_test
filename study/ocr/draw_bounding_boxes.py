import cv2
import matplotlib.pyplot as plt

# pip install matplotlib opencv-python


def draw_bounding_boxes(
    image_path, save_name, bounding_boxes, color=(255, 0, 0), thickness=2
) -> None:
    """
    Draws bounding boxes on an image.

    Parameters:
    - image_path: Path to the image file.
    - bounding_boxes: List of bounding boxes. Each bounding box is represented by
                      a tuple (x1, y1, x2, y2), where (x1, y1) is the top-left
                      corner and (x2, y2) is the bottom-right corner.
    - color: Color of the bounding boxes. Default is red.
    - thickness: Line thickness. Default is 2.

    Returns:
    - Displayed image with bounding boxes.
    """
    # Load the image using OpenCV
    img = cv2.imread(image_path)

    # Convert the image from BGR to RGB format
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    # Draw each bounding box on the image
    for i, box in enumerate(bounding_boxes):
        int_box = [int(x) for x in box]
        x1, y1, x2, y2 = int_box
        cv2.rectangle(img, (x1, y1), (x2, y2), color, thickness)

    # Display the image using matplotlib
    plt.imshow(img)
    plt.axis("off")  # Hide the axis values
    plt.savefig(f"{save_name}.png")
    # plt.show()
