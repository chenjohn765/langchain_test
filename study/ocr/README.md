# Install

```bash
pip install matplotlib opencv-python
```

if dependencies are not installed

# Run study

From the root of backend_python, run

```bash
python3 -m study.ocr.stdy_greedy_grow
```

# Check result

In `study/ocr/study_result` folder, check the intermedary steps of greedy_grow: check how the bounding boxes get merged together.
