"""
This file is to study & display different strategies for merging text boxes in order.
"""


import json
import os
from pathlib import Path
from typing import List

from src.gdrive_loader.azure_vision import azure_read_img
from src.ocr.merge_text_boxes_strategies import (
    greedy_merge_text_boxes,
    min_distance
)
from src.ocr.text_boxes import TextBox
from study.ocr.draw_bounding_boxes import draw_bounding_boxes

output_parent_dir = "study/ocr/study_result"


def study_greedy_grow(
    text_boxes: List[TextBox], img_path: str, output_dir: str
) -> List[TextBox]:
    """
    Same function as src.ocr.merge_text_boxes_strategies.greedy_grow
    but with added code to draw intermediary steps.
    """
    threshold: float = 10
    counter = 0
    while len(text_boxes) > 1:
        text_boxes = greedy_merge_text_boxes(text_boxes, threshold)

        if len(text_boxes) <= 1:
            break

        min_d = min_distance(text_boxes)
        threshold = min_d + 1

        # Draw intermediary result
        counter += 1
        filename = img_path.split("/")[-1].split(".")[0]
        draw_bounding_boxes(
            img_path,
            os.path.join(output_dir, f"{filename}_growing_{counter}"),
            [x.box_raw for x in text_boxes],
        )
    return text_boxes


# ============================= WITH GREEDYGROW =============================

for image_path in [
    "files/img_files/img_complex_1.png",
    "files/img_files/img_complex_2.png",
    "files/img_files/img_complex_3.png",
]:
    filename, extension = image_path.split("/")[-1].split(".")

    output_dir = os.path.join(output_parent_dir, "greedy_grow", f"{filename}")
    Path(output_dir).mkdir(parents=True, exist_ok=True)
    text_boxes = azure_read_img(image_path)

    # Draw the base bboxes obtained by Azure

    draw_bounding_boxes(
        image_path,
        os.path.join(output_dir, f"{filename}_base"),
        [x.box_raw for x in text_boxes],
    )

    new_bounding_boxes = study_greedy_grow(text_boxes, image_path, output_dir)
    draw_bounding_boxes(
        image_path,
        os.path.join(output_dir, f"{filename}_final"),
        [x.box_raw for x in new_bounding_boxes],
    )
    with open(
        os.path.join(output_dir, f"{filename}_final.json"),
        "w",
    ) as f:
        # ensure_ascii = False will avoid encoding characters with accent like é or è
        json.dump([x.content for x in new_bounding_boxes], f, ensure_ascii=False)
